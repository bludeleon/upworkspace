//
//  “This is a course requirement for CS 192 Software Engineering II under the supervision of Asst. Prof. Ma. Rowena C. Solamo of the Department of Computer Science, College of Engineering, University of the Philippines, Diliman for the AY 2017-2018”
//  Authored by Sebastian Morado

//
//  Feb 3, 2018, Sebastian Morado
//  Initial Code


$(function(){
  //February 3, 2018, Sebastian Morado
  //initial code supplying the javascript for changing the pictures on the homepage
  // April 10, 2018
  // Added filter button functionality on homepage google map
  if($('#wifi_value').is(':checked')){
    var pass;
  }else{
    $('#wifi_image').attr("src", "/images/assets-08.png");
    $('#wifi_text').text("Does not need Wifi")
    $('#wifi_text').css({
    "color" : "grey"
    })
  }

  
  if ($('#socket_value').is(':checked')){
    $('#socket_image').attr("src", "/images/assets-07.png");
    $('#socket_text').text("Has Sockets")
    $('#socket_text').css({
    "color" : "black"
    })
  } else {
    $('#socket_image').attr("src", "/images/assets-05.png");
    $('#socket_text').text("Does not need sockets")
    $('#socket_text').css({
    "color" : "grey"
    })
  }

  if ($('#time_value').is(':checked')){
    $('#time_image').attr("src", "./images/assets-09.png");
    $('#time_text').text("Open past 10 pm");
    $('#time_text').css({
    "color" : "black"
    });
  }
  else{
    $('#time_image').attr("src", "/images/assets-10.png");
    $('#time_text').text("No time preference");
    $('#time_text').css({
    "color" : "grey"
    });
  }

  if ($('#seating_value').val() === "1"){
    $('#seating_image').attr("src", "./images/assets-12.png");
    $('#seating_text').text("At most 20 Seating")
    $('#seating_text').css({
    "color" : "black"
    })
  }
  else if ($('#seating_value').val() === "2") {
    $('#seating_image').attr("src", "./images/assets-13.png");
    $('#seating_text').text("At most 40 Seating")
    $('#seating_text').css({
    "color" : "black"
    })
  }
  else if ($('#seating_value').val() === "3") {
    $('#seating_image').attr("src", "./images/assets-14.png");
    $('#seating_text').text("At most 60 Seating")
    $('#seating_text').css({
    "color" : "black"
    })
  }
  else if ($('#seating_value').val() === "0") {
    $('#seating_image').attr("src", "./images/assets-11.png");
    $('#seating_text').text("Any Seating Capacity")
    $('#seating_text').css({
    "color" : "grey"
    })
  }

  if ($('#budget_value').val() === "1"){
    $('#budget_image').attr("src", "./images/assets-16.png");
    $('#budget_text').text("At most 100 pesos")
    $('#budget_text').css({
    "color" : "black"
    });
  }
  else if ($('#budget_value').val() === "2") {
    $('#budget_image').attr("src", "./images/assets-20.png");
    $('#budget_text').text("At most 200 pesos")
    $('#budget_text').css({
    "color" : "black"
    })
  }
  else if ($('#budget_value').val() === "3") {
    $('#budget_image').attr("src", "./images/assets-19.png");
    $('#budget_text').text("At most 300 pesos");
    $('#budget_text').css({
    "color" : "black"
    });
  }
  else if ($('#budget_value').val() === "0") {
    $('#budget_image').attr("src", "./images/assets-15.png");
    $('#budget_text').text("Any Budget");
    $('#budget_text').css({
    "color" : "grey"
    });
  }

  if ($('#popularity_value').val() === "1"){
    $('#popularity_image').attr("src", "./images/assets-02.png");
    $('#popularity_text').text("Not Popular");
    $('#popularity_text').css({
    "color" : "black"
    });
  }
  else if ($('#popularity_value').val() === "2") {
    $('#popularity_image').attr("src", "./images/assets-03.png");
    $('#popularity_text').text("Moderately Popular");
    $('#popularity_text').css({
    "color" : "black"
    });
  }
  else if ($('#popularity_value').val() === "3") {
    $('#popularity_image').attr("src", "./images/assets-04.png");
    $('#popularity_text').text("Very Popular");
    $('#popularity_text').css({
    "color" : "black"
    });
  }
  else if ($('#popularity_value').val() === "0") {
    $('#popularity_image').attr("src", "./images/assets-01.png");
    $('#popularity_text').text("Any Popularity");
    $('#popularity_text').css({
    "color" : "grey"
    });
  }



  $('#search_button1').on('click',function(){
    var key = $('#search2').val().toLowerCase();
    $(".search_entry").each(function( i ) {
      if ($(this).find("#title").text().toLowerCase().includes(key) || $(this).find("#branch").text().toLowerCase().includes(key)){
        $(this).show();
      } else{
        $(this).hide();
      }
    })
  })
  $('#search_button2').on('click',function(){
    $('#search2').val("");
    $(".search_entry").each(function( i ) {
      $(this).show();
    })
  })
  $('#wifi').on('click',function(){
  if ($('#wifi_text').text() === "Has Wifi"){
    $('#wifi_image').attr("src", "/images/assets-08.png");
    $('#wifi_text').text("Does not need Wifi")
    $('#wifi_text').css({
    "color" : "grey"
    })
    $('#wifi_value').prop('checked', false);
  }
  else{
    $('#wifi_image').attr("src", "/images/assets-06.png");
    $('#wifi_text').text("Has Wifi")
    $('#wifi_text').css({
    "color" : "black"
    })
    $('#wifi_value').prop('checked', true);
  }
  })
  $('#socket').on('click',function(){
  if ($('#socket_text').text() === "Has Sockets"){
    $('#socket_image').attr("src", "/images/assets-05.png");
    $('#socket_text').text("Does not need sockets")
    $('#socket_text').css({
    "color" : "grey"
    })
    $('#socket_value').prop('checked', false);
  }
  else{
    $('#socket_image').attr("src", "/images/assets-07.png");
    $('#socket_text').text("Has Sockets")
    $('#socket_text').css({
    "color" : "black"
    })
    $('#socket_value').prop('checked', true);
  }
  })
  $('#time').on('click',function(){
  if ($('#time_text').text() === "Open past 10 pm"){
    $('#time_image').attr("src", "/images/assets-10.png");
    $('#time_text').text("No time preference")
    $('#time_text').css({
    "color" : "grey"
    })
    $('#time_value').prop('checked', false);
  }
  else{
    $('#time_image').attr("src", "./images/assets-09.png");
    $('#time_text').text("Open past 10 pm")
    $('#time_text').css({
    "color" : "black"
    })
    $('#time_value').prop('checked', true);
  }
  })
  $('#seating').on('click',function(){
  if ($('#seating_text').text() === "Any Seating Capacity"){
    $('#seating_image').attr("src", "./images/assets-12.png");
    $('#seating_text').text("At most 20 Seating")
    $('#seating_text').css({
    "color" : "black"
    })
    $('#seating_value').val("1");
  }
  else if ($('#seating_text').text() === "At most 20 Seating") {
    $('#seating_image').attr("src", "./images/assets-13.png");
    $('#seating_text').text("At most 40 Seating")
    $('#seating_text').css({
    "color" : "black"
    })
    $('#seating_value').val("2");
  }
  else if ($('#seating_text').text() === "At most 40 Seating") {
    $('#seating_image').attr("src", "./images/assets-14.png");
    $('#seating_text').text("At most 60 Seating")
    $('#seating_text').css({
    "color" : "black"
    })
    $('#seating_value').val("3");
  }
  else if ($('#seating_text').text() === "At most 60 Seating") {
    $('#seating_image').attr("src", "./images/assets-11.png");
    $('#seating_text').text("Any Seating Capacity")
    $('#seating_text').css({
    "color" : "grey"
    })
    $('#seating_value').val("0");
  }
  })
  $('#budget').on('click',function(){
  if ($('#budget_text').text() === "Any Budget"){
    $('#budget_image').attr("src", "./images/assets-16.png");
    $('#budget_text').text("At most 100 pesos")
    $('#budget_text').css({
    "color" : "black"
    })
    $('#budget_value').val("1");
  }
  else if ($('#budget_text').text() === "At most 100 pesos") {
    $('#budget_image').attr("src", "./images/assets-20.png");
    $('#budget_text').text("At most 200 pesos")
    $('#budget_text').css({
    "color" : "black"
    })
    $('#budget_value').val("2");
  }
  else if ($('#budget_text').text() === "At most 200 pesos") {
    $('#budget_image').attr("src", "./images/assets-19.png");
    $('#budget_text').text("At most 300 pesos")
    $('#budget_text').css({
    "color" : "black"
    })
    $('#budget_value').val("3");
  }
  else if ($('#budget_text').text() === "At most 300 pesos") {
    $('#budget_image').attr("src", "./images/assets-15.png");
    $('#budget_text').text("Any Budget")
    $('#budget_text').css({
    "color" : "grey"
    })
    $('#budget_value').val("0");
  }
  })
  $('#popularity').on('click',function(){
  if ($('#popularity_text').text() === "Any Popularity"){
    $('#popularity_image').attr("src", "./images/assets-02.png");
    $('#popularity_text').text("Not Popular")
    $('#popularity_text').css({
    "color" : "black"
    })
    $('#popularity_value').val("1");
  }
  else if ($('#popularity_text').text() === "Not Popular") {
    $('#popularity_image').attr("src", "./images/assets-03.png");
    $('#popularity_text').text("Moderately Popular")
    $('#popularity_text').css({
    "color" : "black"
    })
    $('#popularity_value').val("2");
  }
  else if ($('#popularity_text').text() === "Moderately Popular") {
    $('#popularity_image').attr("src", "./images/assets-04.png");
    $('#popularity_text').text("Very Popular")
    $('#popularity_text').css({
    "color" : "black"
    })
    $('#popularity_value').val("3");
  }
  else if ($('#popularity_text').text() === "Very Popular") {
    $('#popularity_image').attr("src", "./images/assets-01.png");
    $('#popularity_text').text("Any Popularity")
    $('#popularity_text').css({
    "color" : "grey"
    })
    $('#popularity_value').val("0");
  }
  })
  $('#filter').on('click', function(){
    $('#filter_2').trigger('click');
  })
      
})
//
//  “This is a course requirement for CS 192 Software Engineering II under the supervision of Asst. Prof. Ma. Rowena C. Solamo of the Department of Computer Science, College of Engineering, University of the Philippines, Diliman for the AY 2017-2018”
//  Authored by Sebastian Morado

//
//  Feb 3, 2018, Sebastian Morado
//  Initial Code

//  April 11, 2018
//  Added Google Maps integration

var map, infoWindow;
//Initialize Map - this initializes the google maps integration on the website, taken from GMaps API
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 14.653870, lng: 121.069811},
    zoom: 15
  });
  infoWindow = new google.maps.InfoWindow;

  // Try HTML5 geolocation.
  if (navigator.geolocation) {

    navigator.geolocation.getCurrentPosition(function(position) {
      
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      infoWindow.setPosition(pos);
      infoWindow.setContent('Current Location');
      infoWindow.open(map);
      map.setCenter(pos);
      //
      //POST coordinates of user to return back workspaces that are close to them
      //

      
      var JSONcoords = {
        latitude: position.coords.latitude, 
        longitude: position.coords.longitude
      };
      //Ajax Request that sends user coordinates back to server and then calls postQuery
      var postCoords = $.ajax({
            type: 'POST',
            url: "/stores/near",
            data: JSONcoords,
            dataType: "text",
            success: function(resultData) { console.log(resultData); postQuery(resultData) }
      }); 

      // postCoords.error(function() { alert("Something went wrong"); });
      
      //postQuery - method that takes in data about near workspaces from the server and displays it on the homepage
      function postQuery(resultData){
        var parsed = JSON.parse(resultData)

        if(parsed["stores"].length > 0){
          for(var i=0; i < parsed["stores"].length; i++){
            var lat, long, raw_distance, distance, new_text;
            var wifi_img, socket_img, time_img, seat_img, price_img, pop_img;
            var entry = parsed["stores"][i];
            lat = parseFloat(entry["loc"][1]);
            long = parseFloat(entry["loc"][0]);
            raw_distance = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(14.644663, 121.074051), new google.maps.LatLng(lat, long));
            distance = raw_distance/1000;
            new_text = distance.toFixed(2).toString() + " km away";

            if(entry["has_wifi"]){
              wifi_img = "<img src='/images/wifi.png'>";
            }
            else{
              wifi_img = "<img src='/images/no-wifi.png'>";
            }

            if(entry["has_sockets"]){
              socket_img = "<img src='/images/socket.png'>";
            }
            else{
              socket_img = "<img src='/images/no-socket.png'>";
            }

            if(entry["available_late"]){
              time_img = "<img src='/images/clock.png'>";
            }
            else{
              time_img = "<img src='/images/no-clock.png'>";
            }
            
            if(entry["number_of_chairs"] === 1){
              seat_img = "<img src='/images/seats-1.png'>";
            } else if(entry["number_of_chairs"] === 2){
              seat_img = "<img src='/images/seats-2.png'>";
            } else if(entry["number_of_chairs"] === 3){
              seat_img = "<img src='/images/seats-3.png'>";
            } else if(entry["number_of_chairs"] === 4){
              seat_img = "<img src='/images/seats-3.png'>";
            } else{
              seat_img = "<img src='/images/seats-0.png'>";
            }

            if(entry["price"] === 1){
              price_img = "<img src='/images/money-1.png'>";
            } else if(entry["price"] === 2){
              price_img = "<img src='/images/money-2.png'>";
            } else if(entry["price"] === 3){
              price_img = "<img src='/images/money-3.png'>";
            } else if(entry["price"] === 4){
              price_img = "<img src='/images/money-3.png'>";
            }

            if(entry["rating"] === 1){
              pop_img = "<img src='/images/stars-1.png'>";
            } else if(entry["rating"] === 2){
              pop_img = "<img src='/images/stars-2.png'>";
            } else if(entry["rating"] === 3){
              pop_img = "<img src='/images/stars-3.png'>";
            }


            $('#map_results').append("\
            <a class='row search_entry' href='/stores/" + entry["_id"] + "'>\
              <div class='col-md-12 entry'>\
                <div class='entry_top'>\
                  <div class='entry_head'>\
                    <span id='title'>" + entry["name"] + "</span>\
                    <span id='branch'>" + entry["short_address"] + "</span>\
                  </div>\
                  <div class='entry_distance'>\
                    <span id='distance'>" + new_text +"</span>\
                  </div>\
                </div>\
                <div class='type'>"
                  + entry["workspace_type"] + "\
                </div>\
                <div class='filters'>" + wifi_img + socket_img + time_img + seat_img + price_img + pop_img + "\
                </div>\
              </div>\
            </a>");

            

            marker = new google.maps.InfoWindow({
              content: entry["name"],
              position: new google.maps.LatLng(lat, long)
            });
            marker.open(map);
          }
        } else{
          $('#map_results').append("\
            <div class='row'>\
              <div class='col-md-12'>\
                <h2 class='text-align'></h2>\
              </div>\
            </div>")
        }
        $('#maps_container').show();
      }
      

    }, function() {
      handleLocationError(true, infoWindow, map.getCenter());
    });
  } else {
    // Browser doesn't support Geolocation
    handleLocationError(false, infoWindow, map.getCenter());
  }
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' :
                        'Error: Your browser doesn\'t support geolocation.');
  infoWindow.open(map);
}

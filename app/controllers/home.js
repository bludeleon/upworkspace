/*
  “This is a course requirement for CS 192 Software Engineering II under the supervision of Asst. Prof. Ma. Rowena C. Solamo of the Department of Computer Science, College of Engineering, University of the Philippines, Diliman for the AY 2017-2018”

  Authored by Edmund Alwin D. de Leon
*/

/*
2/2/18
added routes to pages and route to create user
*/
/*
3/9/18
added routes to pages and route to create user
*/
/*
3/22/18
added routes to pages and route to create user\
added create and update store routes
*/



var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Store = mongoose.model('Store');


function auth( req, res, next ){
  return next(); //remove after development
  if(!req.session.email) return res.redirect('/');
  return next();
};


module.exports = function (app) {
  app.use('/', router);
};

//edited route for testing: made it so it gave all db entries instead of those that are close
router.get('/', function (req, res, next) {
    console.log("SESSION STATUS:", req.session.isLogin)
    return res.render('home', {isLogin:req.session.isLogin});
});

router.get('/registration', function(req, res, next){
  console.log("SESSION STATUS:", req.session.isLogin)
  return res.render('registration',{isLogin: req.session.isLogin});
});

router.get('/signin', function(req, res, next){
  return res.render('signin',{isLogin: req.session.isLogin});
});

//April 10, 2018, Sebastian Morado
//Added route leading to About Page
router.get('/about', function(req, res, next){
  return res.render('about',{isLogin: req.session.isLogin});
});

router.get('/search_results', function(req, res, next){
  console.log("SESSION STATUS:", req.session.isLogin)
  return res.render('search_results',{isLogin: req.session.isLogin});
});

//March 7, 2018, Sebastian Morado
//Added route 'test' as the sample workspace page, needs to be replaced with actual dynamic route
router.get('/test', function(req, res, next){
  console.log("SESSION STATUS:", req.session.isLogin)
  return res.render('workspace_page',{isLogin: req.session.isLogin});
});
//Added route 'add' which should lead to the add workspace page. needs to be accessible only if youre logged in
router.get('/add', function(req, res, next){
  console.log("SESSION STATUS:", req.session.isLogin)
  return res.render('add',{isLogin: req.session.isLogin});
});

router.get('/stores/:_id', function(req, res, next){
  Store.findOne({_id: req.params._id}, function(err, store){
    console.log(store);
    return res.render('workspace_page',{store: store, isLogin: req.session.isLogin})
  })
});




//March 21, 2018, Sebastian Morado
//Added update route for updating workspace page, also fixed some session details
router.get('/update/:_id', function(req, res, next){
  Store.findOne({_id: req.params._id}, function(err, store){
    console.log(store)
    return res.render('update',{store: store, isLogin: req.session.isLogin})
  })
});



router.get('/logout', function (req, res, next) {
    req.session.destroy();
    res.redirect('/');
});

router.post('/login', function(req, res, next){
  var user_fields = req.body;
  console.log("BODY: ", user_fields)
  User.findOne({email: user_fields.email}, function(err, user){
    console.log("USER: ", user)
    if(user == null ){
      return res.render('signin',{message:"Email address is not registered"});
    }else if (user.password == user_fields.password){
      //successful login
      req.session.email = user.email;
      req.session.isLogin = true;
      return res.redirect('/');
    }else {
      return res.render('signin',{message:"Invalid email or password"});
    }
    
  }) 
});

router.post('/registration', function(req, res, next){
  var user_fields = req.body;
  console.log(user_fields)

  User.findOne({email: user_fields.email}, function(err, user){
    console.log("USER: ", user)
    if(user == null ){
      User.create(user_fields, function(err, user){
        console.log(user)
        return res.render('signin', {message:"account created!"});
      })
    }else{
      return res.render('registration',{message:"Email is already used"});

    }
    
  }) 
});

router.post('/search', function(req, res, next){
  var store_name = req.body.store_name;
  console.log(store_name)

  Store.find({ $or: [{name: { $regex: store_name, $options: 'i' }}, {short_address:  { $regex: store_name, $options: 'i' }}, {branch:  { $regex: store_name, $options: 'i' }}] }, function(err, stores){
    return res.render('search_results', {stores: stores, isLogin:req.session.isLogin});
    
  }) 
});

router.post('/add/store', function(req, res, next){

  var rawNewStore = req.body
  console.log(rawNewStore)
  rawNewStore.hours = [rawNewStore.sunday_hours, rawNewStore.monday_hours, rawNewStore.tuesday_hours, rawNewStore.wednesday_hours, rawNewStore.thursday_hours, rawNewStore.friday_hours, rawNewStore.saturday_hours]
  rawNewStore.short_address = rawNewStore.branch
  rawNewStore.is_open = true
  // set price
  if(rawNewStore.min_budget <= 100){
    rawNewStore.price = 1
  }else if( rawNewStore.min_budget <= 200){
    rawNewStore.price = 2
  }else if( rawNewStore.min_budget <= 300){
    rawNewStore.price = 3
  }else {
    rawNewStore.price = 4
  }

  // set number of seats
  if(rawNewStore.capacity <= 20){
    rawNewStore.number_of_chairs = 1
  }else if( rawNewStore.capacity <=40){
    rawNewStore.number_of_chairs = 2
  }else if( rawNewStore.capacity <= 60){
    rawNewStore.number_of_chairs = 3
  }else{
    rawNewStore.number_of_chairs = 4;
  }

  // set available late
  var temp_latest_time_open = rawNewStore.latest_time_open
  temp_latest_time_open.substring(0,2)
  temp_latest_time_open = parseInt(temp_latest_time_open);
  if(temp_latest_time_open<22){
    rawNewStore.available_late = false
  }else{
    rawNewStore.available_late = true
  }

  rawNewStore.loc = [  parseFloat(rawNewStore.longitude), parseFloat(rawNewStore.latitude) ];

  var newStore = rawNewStore
  Store.create( newStore, function(err, store){
    console.log("error: ", err);
    console.log("new store: ",store)
    return res.redirect('/stores/'+ store._id);
  });
});

router.post('/search-filter', function(req, res, next){
  console.log(req.body)
  if(req.body.has_wifi == null){
    var cur_has_wifi = false
  }else{
    var cur_has_wifi = true
  }

  if(req.body.has_sockets == null){
    var cur_has_sockets = false
  }else{
    var cur_has_sockets = true
  }

  if(req.body.time == null){
    var cur_time = false
  }else{
    var cur_time = true
  }

  console.log({
    has_wifi: cur_has_wifi,
    has_sockets: cur_has_sockets,
    available_late: cur_time,
    number_of_chairs: req.body.seats,
    price: req.body.price,
    rating: req.body.rating
  })

  var cur_query = {
    has_wifi: cur_has_wifi,
    has_sockets: cur_has_sockets,
    available_late: cur_time,
    number_of_chairs: {$lte:Number(req.body.seats)},
    price: {$lte:Number(req.body.price)},
    rating: req.body.rating
  }


  if( cur_query.has_wifi == false){
    delete cur_query.has_wifi;
  }

  if( cur_query.has_sockets == false){
    delete cur_query.has_sockets;
  }

  if( cur_query.available_late == false){
    delete cur_query.available_late;
  }

  if( cur_query.number_of_chairs == 3){
    cur_query.number_of_chairs = 3;
  }

  if( cur_query.price == 3){
    cur_query.price = 3;
  }

  if(cur_query.price.$lte == 0 ){
    delete cur_query.price;
  }

  if(cur_query.number_of_chairs.$lte == 0 ){
    delete cur_query.number_of_chairs;
  }

  if(cur_query.rating == 0 ){
    delete cur_query.rating;
  }
  if(cur_query.rating == undefined){
    delete cur_query.rating
  }
  console.log(cur_query)
  Store.find(cur_query, function(err, stores){
    if(err) return res.send(err)
    if(stores.length == 0) {
      return res.render('search_results',{stores:[]})
    }
    return res.render('search_results',{stores: stores, isLogin:req.session.isLogin})
  })

});

router.post('/stores/near',function(req, res, next){
  console.log(req.body)
  Store.find({
      loc:{
        $near:{
         $geometry: {
            type: "2d" ,
            coordinates: [ parseFloat(req.body.longitude) , parseFloat(req.body.latitude) ]
         },
         $maxDistance : 1000
       }
      }
    },
    function(err, stores){
      if(err) return res.send({message: err});
      return res.json({stores:stores});
    })
});

// UPDATES

router.post('/update/store/:store_id',function(req, res, next){
  var rawNewStore = req.body
  console.log(rawNewStore)
  rawNewStore.hours = [rawNewStore.sunday_hours, rawNewStore.monday_hours, rawNewStore.tuesday_hours, rawNewStore.wednesday_hours, rawNewStore.thursday_hours, rawNewStore.friday_hours, rawNewStore.saturday_hours]
  rawNewStore.short_address = rawNewStore.branch
  rawNewStore.is_open = true
  // set price
  if(rawNewStore.min_budget <= 100){
    rawNewStore.price = 1
  }else if( rawNewStore.min_budget <=200){
    rawNewStore.price = 2
  }else if( rawNewStore.min_budget <= 300){
    rawNewStore.price = 3
  }else{
    rawNewStore.price = 4
  }

  // set number of seats
  if(rawNewStore.capacity <= 20){
    rawNewStore.number_of_chairs = 1
  }else if( rawNewStore.capacity <=40){
    rawNewStore.number_of_chairs = 2
  }else if ( rawNewStore.capacity <= 60){
    rawNewStore.number_of_chairs = 3
  } else{
    rawNewStore.number_of_chairs = 4
  }


  // set available late
  var temp_latest_time_open = rawNewStore.latest_time_open
  temp_latest_time_open.substring(0,2)
  temp_latest_time_open = parseInt(temp_latest_time_open);
  if(temp_latest_time_open<22){
    rawNewStore.available_late = false
  }else{
    rawNewStore.available_late = true
  }

  // has_wifi
  if(rawNewStore.has_wifi == undefined){
    rawNewStore.has_wifi = false
  }
  // has sockets
  if(rawNewStore.has_sockets == undefined){
    rawNewStore.has_sockets = false
  }

  rawNewStore.loc = [rawNewStore.longitude, rawNewStore.latitude ]

  var newStore = rawNewStore

  console.log("params id: ", req.params.store_id)
  Store.findOneAndUpdate({_id: req.params.store_id}, newStore, function(err, store){
    console.log(err);
    console.log("updated store: ",store);
    return res.redirect('/stores/'+ store._id);
  });
});
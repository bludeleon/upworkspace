// Example model

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var StoreSchema = new Schema({
  name: String,
  short_address: String,
  long_address: String,
  description: String,
  branch: String,
  workspace_type: String,
  latitude: Number,
  longitude: Number,
  hours: [String],
  latest_time_open: String,
  has_wifi: Boolean,
  has_sockets: Boolean,
  available_late: Boolean,
  number_of_chairs: Number,
  price: Number,
  rating: Number,
  distance: Number,
  type_of_store: String,
  image_link: String,
  is_open: Boolean,
  min_budget: Number,
  capacity: Number,
  loc: {type: [Number], index:'2dsphere'}
});



mongoose.model('Store', StoreSchema);

